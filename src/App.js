import Home from "./components/Home";
import "./App.css";
import Footer from "./components/Footer";

function App() {
    return (
        <div>
            <Home />
            <Footer />
        </div>
    );
}

export default App;
